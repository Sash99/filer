<?php 

	class BaseModel{

		private $db;
		function __construct(){
			session_start();
			$this->db = new mysqli("localhost","root","","das_13");
			if(isset($_POST["action"])){
				if($_POST["action"]=="ajax"){
					$this->register();
				}
				if($_POST["action"]=="editprof"){
					$this->edit();
				}
				if($_POST["action"]=="search"){
					$this->search();
				}
				if($_POST["action"]=="addFr"){
					$this->addFriend();
				}
				if($_POST["action"]=="notifications"){
					$this->notific();
				}
				if($_POST["action"]=="accept"){
					$this->accept();
				}
				if($_POST["action"]=="delete"){
					$this->delete();
				}
				if($_POST["action"]=="remove"){
					$this->remove();
				}
				if($_POST["action"]=="friends"){
					$this->friends();
				}
				if($_POST["action"]=="message"){
					$this->message();
				}
				if($_POST["action"]=="show_photo"){
					$this->addPhoto();
				}
				if($_POST["action"]=="getMessage"){
					$this->getMessage();
				}
				if($_POST["action"]=="sendMsg"){
					$this->sendMessage();
				}
				if($_POST["action"]=="deletePhoto"){
					$this->deletePhoto();
				}
				if($_POST["action"]=="addPost"){
					$this->showPost();
				}
				if($_POST["action"]=="setPhoto"){
					$this->setPhoto();
				}
                if($_POST["action"]=="deletePost"){
					$this->deletePost();
				}
                if($_POST["action"]=="deletefrend"){
					$this->deletefrend();
				}
                if($_POST["action"]=="inform"){
					$this->meinform();
				}
                if($_POST["action"]=="addinform"){
					$this->showinform();
				}
                if($_POST["action"]=="deleteinform"){
					$this->deleteinform();
				}
                if($_POST["action"]=="comments"){
					$this->comments();
				}
                if($_POST["action"]=="addcomments"){
					$this->addcomments();
				}
                
                

			}
			if(isset($_POST["save"])){
				$this->login();
			}
			if(isset($_FILES["upload"])){
					$this->upload();
				}
			if(isset($_POST["publish"])){
				$this->publishPost();
			}
		}
		function register(){
			$name = $_POST["name"];
			$surname = $_POST["surname"];
			$age = $_POST["age"];
			$email = $_POST["email"];
			$pass = $_POST["pass"];
			$conpass = $_POST["conpass"];
			$errors = [];
			$exist_email = $this-> db-> query("SELECT email FROM users WHERE email='$email'")->fetch_all(true);

			if(empty($name)){
				$errors["name"]="Լրացնել Անուն․";
			}
			if(empty($surname)){
				$errors["surname"]= "Լրացնել Ազգանուն․";
			}
			if(empty($age)){
				$errors["age"] = "Լրացնել Տարիք․";
			}
			else if(!filter_var($age,FILTER_VALIDATE_INT)){
				$errors["age"] = "Լրացնել Պարտադիր Թիվ․";
			
			}
			if(empty($email)){
				$errors["email"] = "Լրացնել Էլ․Հասցե․";
			}
			else if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
				$errors["email"] = "Այսպիսի Էլ․Հասցե Գոյություն Չունի․";
			
			}
			else if(count($exist_email)>0){
				$errors["email"]="Էլ․ Հասցեն Արդեն Գրանցված է․";
			}
			if(empty($pass)){
				$errors["pass"] = "Լրացնել Գախնաբառ․";
			}
			else if(strlen($pass)<5){
				$errors["pass"] = "Գրել 5-ից Ոչ պակաս․";
			
			}
			if(empty($conpass)){
				$errors["conpass"] = "Գախնաբառերը Չեն Համընկնում․";
			}
			else if($pass != $conpass){
				$errors["pass"] = "Գախնաբառի Մեջ Սխալ Կա․";
			
	    	} 
			if(count($errors)>0){
				print json_encode($errors);
			}
			else{
				$pass = password_hash($pass, PASSWORD_DEFAULT);
				$this->db->query("INSERT INTO users (name,surname,age,email,password,photo) VALUES
					('$name','$surname','$age','$email','$pass','uploads/icon.png')");
			}
		}
		function login(){
			$email = $_POST["email"];
			$password = $_POST["password"];
			$errors = [];
			$data = $this->db->query("SELECT * from users where email = '$email'")->fetch_all(true);
			if(empty($email)){
				$errors["email"]=" Լրացնել Էլ․Հասցե․";
			}
			if(empty($password)){
				$errors["password"]="Լրացնել Գախնաբառ․";
			}
			else if(empty($data)){
				$errors["email"]= "Լրացնել Էլ․Հասցեն Սխալ է․";
			}
			else {
				if(password_verify($password,$data[0]["password"])) {
					$_SESSION['user']=$data[0];
					header('location:profile.php');
				}
					else{
						$error["password"]="Գախնաբառը Սխալ է․";
					}
			}
			
			 if(count($errors)>0){
					$_SESSION["error"] = $errors;
					header("location:login.php");
				}
			
			
		}

		function edit(){
            
			$name = $_POST['name'];
			$surname = $_POST['surname'];
			$age = $_POST['age'];
			$id = $_SESSION['user']["ID"];
			$this->db->query("UPDATE users SET name = '$name',surname = '$surname', age = '$age' WHERE ID = '$id'");
			$data = $this->db->query("SELECT * FROM users WHERE ID = '$id'")->fetch_all(true);
			$_SESSION["user"] = $data[0];

		}

		function search(){


			$s = $_POST["name"];
			$data = $this->db->query("SELECT * FROM users WHERE name like'%$s%' ")->fetch_all(true);
			$id = $_SESSION['user']["ID"];
			$arr = [];
			foreach ($data as $key) {
				$user_id = $key["ID"];
				$req = $this->db->query("SELECT * FROM requests WHERE my_id = '$id' and user_id = '$user_id'")->fetch_all(true); 
				$fr = $this->db->query("SELECT * FROM friends WHERE (my_id = '$id' and user_id = '$user_id')
					or (user_id='$id' and my_id = '$user_id')")->fetch_all(true);
				if($user_id == $id){
					$key["status"] = 0;
				}
				else if(!empty($req)){
					$key["status"] = 1;
				}
				else if(!empty($fr)){
					$key["status"]=2;
				}
				else{
					$key["status"]=3;
				}
				$arr[] = $key;
				
			}
				print json_encode($arr);

		}
		function addFriend(){
			$user_id = $_POST['user_id'];
			$id = $_SESSION['user']["ID"];
			$this->db->query("INSERT INTO requests (my_id,user_id) VALUES ('$id','$user_id')");

		}
		function notific(){

			$id = $_SESSION['user']["ID"];
			$data = $this->db->query("SELECT * FROM requests JOIN users on requests.my_id = users.ID where user_id = '$id'")->fetch_all(true);
			print json_encode($data);

		}
		function upload(){
			$id = $_SESSION['user']["ID"];
			$k = $_FILES["upload"];
			print_r($k);
			$tmp = $k['tmp_name'];
			if(!empty($k['name'])){

			$address = "uploads/".time().$k["name"];
			}
			if (!file_exists("uploads")) {
	
    				 mkdir("uploads");

				}

				move_uploaded_file($tmp,$address);	
			
				header("location:photos.php");

				$this->db->query("INSERT INTO photo (User_id ,Photo_url)  VALUES ('$id','$address')");

		}
		

		function accept(){
			$id = $_SESSION['user']["ID"];
			$user_id = $_POST['data_id'];
			$this->db->query("INSERT INTO friends (my_id,user_id) VALUES ('$id','$user_id')");
			$this->db->query("DELETE FROM requests where my_id='$user_id' and user_id='$id'");
		}
		function delete(){
			$id = $_SESSION['user']["ID"];
			$user_id = $_POST['data_id']; 
			$this->db->query("DELETE FROM requests where my_id='$user_id' and user_id='$id'");
		}
		function remove(){
			$user_id = $_POST['data_id'];
			$id = $_SESSION['user']["ID"];
			$this->db->query("DELETE FROM requests where my_id='$id' and user_id='$user_id'");
		}
		function friends(){
			$id = $_SESSION['user']["ID"];
			$data = $this->db->query("SELECT * FROM users where ID in(SELECT my_id from friends where user_id = '$id' 
									union SELECT user_id from friends where my_id = '$id')")->fetch_all(true);
			print json_encode($data);

		}
		function message(){
			$user_id = $_POST['data_id'];
			$data = $this->db->query("SELECT * FROM users where ID ='$user_id'")->fetch_all(true);
			print json_encode($data);

		}
		function addPhoto(){
			$id = $_SESSION['user']["ID"];
			$data = $this->db->query("SELECT * FROM photo WHERE User_id = '$id'")->fetch_all(true);
			print json_encode($data);

		}
		function getMessage(){
			$id=$_POST["id"];
			$my_id=$_SESSION['user']['ID'];
			$data=$this->db->query("SELECT * FROM messages where (my_id='$my_id' and rec_id='$id') or
					              (my_id='$id' and rec_id='$my_id')")->fetch_all(true);
				print json_encode($data);
		}
		function sendMessage(){
			$id = $_SESSION['user']["ID"];
			$user_id = $_POST['id'];
			$msg = $_POST['msg'];
			$this->db->query("INSERT INTO messages (rec_id,my_id,message) VALUES ('$user_id','$id','$msg')");

		}
		function deletePhoto(){
			$id = $_POST['data_id'];
			$this->db->query("DELETE FROM photo WHERE ID = '$id'");

		}

		function publishPost(){
			$id = $_SESSION['user']["ID"];
			$post = $_POST["post_desc"];
			$pic = $_FILES["post_pic"];			
            


			   if(!empty($pic["name"])){

			   	$tmp = $pic["tmp_name"];
			   	$address = "uploads/".time().$pic["name"];

			   	if(!file_exists("uploads")){
			   		mkdir("uploads");
			   	 }
			   	move_uploaded_file($tmp,$address);

			    $this->db->query("INSERT INTO post (User_id,Post,Picture) VALUES ('$id','$post','$address')");

			  }


			else{
				$this->db->query("INSERT INTO post (User_id,Post) VALUES ('$id','$post')");
			}
			header("location:profile.php");
		}

		function showPost(){
			$id = $_SESSION['user']["ID"];
			$data = $this->db->query("SELECT * FROM post WHERE User_id = '$id' order by time")->fetch_all(true);
			print json_encode($data);

		}

		function setPhoto(){
			$id = $_SESSION['user']["ID"];
        	$data_id = $_POST["data_id"];
			$this->db->query("UPDATE users SET photo = '$data_id' where ID = '$id' ");
			$data = $this->db->query("SELECT photo FROM users WHERE ID = '$id'")->fetch_all(true);
			print_r($data[0]['photo']);
			$_SESSION['user']['photo'] = $data[0]['photo'];
		}
		function deletePost(){
			$id = $_POST['data_idd'];
			$this->db->query("DELETE FROM post WHERE ID = '$id'");

		}
        function deletefrend(){
			$id = $_SESSION['user']["ID"];
			$user_id = $_POST['data_iddd']; 
			$this->db->query("DELETE FROM friends where (user_id='$user_id' and my_id='$id') or (my_id='$user_id' and user_id='$id')");
		}
        function meinform(){
            $id = $_SESSION['user']["ID"];
			$post = $_POST["inf_val"];
            $this->db->query("INSERT INTO Inform (inform_id,inform) VALUES ('$id','$post')");
        } 
       function showinform(){
            $idd = $_SESSION['user']["ID"];
			$dat = $this->db->query("SELECT * FROM Inform WHERE inform_id = '$idd'")->fetch_all(true);
			print json_encode($dat);

       }
        function deleteinform(){
            $id = $_POST['data_iddd'];
			$this->db->query("DELETE FROM inform WHERE ID = '$id'");

       }
        
        
        function comments(){
            $post_id=$_POST['id_posts'];
            $user_id=$_SESSION['user']['ID'];
            $this->db->query("INSERT INTO comments (post_id,user_id) VALUES ('$post_id','$user_id')");        
        }
        function addcomments(){
            $iddd = $_SESSION['user']["ID"];
			$date = $this->db->query("SELECT * FROM comments WHERE user_id = '$iddd'")->fetch_all(true);
			print json_encode($date);

        }
}


	$m = new BaseModel();


 ?>
