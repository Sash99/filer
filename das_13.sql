/*
 Navicat Premium Data Transfer

 Source Server         : Arman
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : das_13

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 13/08/2019 21:42:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for friends
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `cdhf`(`my_id`) USING BTREE,
  INDEX `fvf`(`user_id`) USING BTREE,
  CONSTRAINT `cdhf` FOREIGN KEY (`my_id`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fvf` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of friends
-- ----------------------------
INSERT INTO `friends` VALUES (64, 20, 14);
INSERT INTO `friends` VALUES (65, 20, 21);

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `rec_id` int(11) NULL DEFAULT NULL,
  `my_id` int(11) NULL DEFAULT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `rec_id`(`rec_id`) USING BTREE,
  INDEX `my_id`(`my_id`) USING BTREE,
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`rec_id`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`my_id`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES (49, 20, 14, 'barev', '2019-08-13 19:59:28');
INSERT INTO `messages` VALUES (50, 21, 20, 'ok', '2019-08-13 19:59:47');
INSERT INTO `messages` VALUES (51, 21, 20, 'ghfgv', '2019-08-13 20:03:03');

-- ----------------------------
-- Table structure for photo
-- ----------------------------
DROP TABLE IF EXISTS `photo`;
CREATE TABLE `photo`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Photo_url` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `User_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `sa`(`User_id`) USING BTREE,
  CONSTRAINT `sa` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `User_id` int(11) NULL DEFAULT NULL,
  `Post` varchar(1500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Picture` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Time` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `User_id`(`User_id`) USING BTREE,
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`User_id`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES (1, 20, 'ssssssss', 'uploads/1565715698WhatsApp Image 2019-06-12 at 10.59.45.jpeg', '2019-08-13 21:01:38');
INSERT INTO `post` VALUES (2, 20, 'ssssssssssssss', NULL, '2019-08-13 21:02:08');
INSERT INTO `post` VALUES (3, 20, 'bbbbbbbbb', NULL, '2019-08-13 21:02:49');
INSERT INTO `post` VALUES (4, 20, 'www', 'uploads/1565715783WhatsApp Image 2019-06-12 at 10.59.45.jpeg', '2019-08-13 21:03:03');
INSERT INTO `post` VALUES (5, 20, 'my name is Van Damme', 'uploads/1565717567IMG_5857mk.jpg', '2019-08-13 21:32:47');

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `my_id`(`my_id`) USING BTREE,
  CONSTRAINT `requests_ibfk_1` FOREIGN KEY (`my_id`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (14, 'Samvel', 'Kostanyan', 25, 'sam@mail.ru', '$2y$10$MWe3r7AkcJZukBj2FMaWoeoUNW92r1M4CcyvwzzIclreKwtNbW6u6');
INSERT INTO `users` VALUES (20, 'Arman', 'Muradkhanyan', 27, 'abc@mail.ru', '$2y$10$FE3cMGnATf3F64iRNSpQbuDFeBmCPa1Nll.YduW95dP2uRww/91Qu');
INSERT INTO `users` VALUES (21, 'Valod', 'Petrosyan', 25, 'aabb@mail.ru', '$2y$10$d/LIDnAHzYsV9ga4iwzrU.d2KoHIR0T9f5WwYnp0uhFOP3iEGpe.m');

SET FOREIGN_KEY_CHECKS = 1;
